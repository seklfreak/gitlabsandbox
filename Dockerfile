# build
FROM golang:1.11-stretch as builder

RUN mkdir /app
ADD . /app/
WORKDIR /app

RUN make all

# run
FROM ubuntu:bionic

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
&& rm -rf /var/lib/apt/lists/*

EXPOSE 8080
ENTRYPOINT [ "/badbot" ]
COPY --from=builder "/app/badbot" /