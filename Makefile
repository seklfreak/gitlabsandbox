.PHONY: all
all: clean build

.PHONY: lint
lint:
	@go vet ./...

.PHONY: test
test:
	@go test -v ./...

.PHONY: race
race:
	@go test -v -race ./...

.PHONY: coverage
coverage:
	@go test -v -covermode=count -coverprofile=coverage.cov ./...
	@go tool cover -func=coverage.cov

.PHONY: coverage-html
coverage-html:
	@go test -v -covermode=count -coverprofile=coverage.cov ./...
	@go tool cover -func=coverage.cov
	@go tool cover -html=coverage.cov -o coverage.html

.PHONY: build
build:
	@go build -i -v ./cmd/badbot

.PHONY: clean
clean:
	@if [ -a badbot ]; then rm badbot; fi
	@if [ -a coverage.cov ]; then rm coverage.cov; fi
	@if [ -a coverage.html ]; then rm coverage.html; fi
