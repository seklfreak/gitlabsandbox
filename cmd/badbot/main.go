package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/prometheus/client_golang/prometheus"

	unleash "github.com/Unleash/unleash-client-go"
	"github.com/bwmarrin/discordgo"
)

var (
	discordToken      = os.Getenv("DISCORD_TOKEN")
	unleashAppName    = os.Getenv("UNLEASH_APP_NAME")
	unleashURL        = os.Getenv("UNLEASH_URL")
	unleashInstanceID = os.Getenv("UNLEASH_INSTANCE_ID")

	metricCommandsCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "production",
		Subsystem: "gitlabsandbox",
		Name:      "commands_counter",
	})
)

func main() {
	prometheus.MustRegister(metricCommandsCounter)

	err := unleash.Initialize(
		unleash.WithListener(&unleash.DebugListener{}),
		unleash.WithAppName(unleashAppName),
		unleash.WithUrl(unleashURL),
		unleash.WithInstanceId(unleashInstanceID),
	)
	check(err)

	dgSession, err := discordgo.New("Bot " + discordToken)
	check(err)

	dgSession.AddHandler(messageCreate)

	err = dgSession.Open()
	check(err)

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
	}()

	fmt.Println("Bad Bot started")

	exitChannel := make(chan os.Signal, 1)
	signal.Notify(exitChannel, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-exitChannel

	err = dgSession.Close()
	check(err)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func messageCreate(dgSession *discordgo.Session, message *discordgo.MessageCreate) {
	if message.Author.Bot {
		return
	}

	switch strings.ToLower(strings.TrimSpace(message.Content)) {
	case "ping":
		if !unleash.IsEnabled("cmd_ping_enabled") {
			return
		}
		dgSession.ChannelMessageSend(message.ChannelID, "Pong!")
		metricCommandsCounter.Inc()
	case "pong":
		if !unleash.IsEnabled("cmd_pong_enabled") {
			return
		}

		dgSession.ChannelMessageSend(message.ChannelID, "Ping!")
		metricCommandsCounter.Inc()
	}
}
