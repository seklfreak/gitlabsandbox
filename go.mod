module main

require (
	github.com/Unleash/unleash-client-go v0.0.0-20181121205122-ae068e0ad68c
	github.com/bwmarrin/discordgo v0.19.0
	github.com/prometheus/client_golang v0.9.2
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9 // indirect
	gopkg.in/h2non/gock.v1 v1.0.12 // indirect
)
